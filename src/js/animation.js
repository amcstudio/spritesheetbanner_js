~ function () {
	'use strict';
	var $ = TweenMax,
		ad = document.getElementById('mainContent'),
		position,
		spriteDiff,
		setInt;



	window.init = function () {
		play();	
	}

	function play() {

		var tl = new TimelineLite();

		tl.add(spritesheetAnimation)
		tl.to(['.bgCircle','#logo', '#endCopy'] , 0.5,{ opacity: 1, ease:Power1.easeInOut },'+=3')

	}

	function stopAnimate() {
		clearInterval(setInt);
	  } 

	  
	  function spritesheetAnimation() {
		position =200; 
		spriteDiff = 200; 
		
		setInt = setInterval(function(){  
		  document.getElementById("spriteSheet").style.backgroundPositionX = -1 * position+'px';
		 
		  if (position < 12000) {
			position = position + spriteDiff;
		  }
		  else {
			position = 200;
			stopAnimate();
		  }		 		  
		}, 50); 
	  } 

}();

